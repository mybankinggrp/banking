package main

import (
	"log"

	"gitlab.com/mybankinggrp/banking/app"
)

func main() {
	log.Println("Starting Server")
	log.Println("Started")
	app.Start()

}
